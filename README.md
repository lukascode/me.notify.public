# What is this repo all about?
This repo hold files that we want help on like supported platforms.

# Platforms
This is how we format a platform object and what we use the data for

### Here is an example platform
```javascript
youtube: {
    key: 'youtube',
    name: 'YouTube',
    url: 'youtube.com',
    hex: '#ff0000',
    wideThumb: true,
    urlMatch: /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
    extract: (link) => link.match(/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/).filter(result => result.length === 11)[0],
},
```

### Here is what everything is used for
```
key: The key is what we use to get the icon files
name: This is the display name for the platform
url: The base domain for the platform
hax: The primary color for the platform
wideThumb: This determines if the platform's thumbnails takes up the whole post image
urlMatch: This is the regex that we use to determine whether a url belongs to this platform
```
extract: This is a function which used a regex to extract content from the url