
const platforms = {

    youtube: {
        key: 'youtube',
        name: 'YouTube',
        url: 'youtube.com',
        hex: '#ff0000',
        wideThumb: true,
        urlMatch: /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
        extract: (link) => link.match(/^.*(youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/).filter(result => result.length === 11)[0],
    },
    twitch: {
        key: 'twitch',
        name: 'Twitch',
        url: 'twitch.tv',
        hex: '#6441A4',
        wideThumb: true,
        urlMatch: /http(?:s)?:\/\/(?:www\.)?twitch\.tv\/([a-zA-Z0-9_]+)/,
        extract: (link) => link.split('twitch.tv/')[1].split('/')[0],
    },
    soundcloud: {
        key: 'soundcloud',
        name: 'SoundCloud',
        url: 'soundcloud.com',
        hex: '#ff8800',
        wideThumb: false,
        urlMatch: /((https:\/\/)|(http:\/\/)|(www.)|(m\.)|(\s))+(soundcloud.com\/)+[a-zA-Z0-9\-\.]+(\/)+[a-zA-Z0-9\-\.]+/,
        extract: null,
    },
    spotify: {
        key: 'spotify',
        name: 'Spotify',
        url: 'spotify.com',
        hex: '#1DB954',
        wideThumb: false,
        urlMatch: /http(?:s)?:\/\/(?:www\.)?open.spotify\.com\/([a-zA-Z0-9_]+)/,
        extract: null,
    },
    twitter: {
        key: 'twitter',
        name: 'Twitter',
        url: 'twitter.com',
        hex: '#00aced',
        wideThumb: false,
        urlMatch: /http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/,
        extract: null,
    },
    instagram: {
        key: 'instagram',
        name: 'Instagram',
        url: 'instagram.com',
        hex: '#e33567',
        wideThumb: false,
        urlMatch: /http(?:s)?:\/\/(?:www\.)?instagram\.com\/([a-zA-Z0-9_]+)/,
        extract: null,
    },
    notify: {
        key: 'notify',
        name: 'Notify',
        url: 'notify.me',
        hex: '#1bb9dc',
        wideThumb: false,
        urlMatch: /http(?:s)?:\/\/(?:blog\.)?notify\.me\/([a-zA-Z0-9_]+)/,
        extract: null,
    },
    discord: {
        key: 'discord',
        name: 'Discord',
        url: 'notify.me',
        hex: '#7289da',
        wideThumb: false,
        urlMatch: /http(?:s)?:\/\/(?:www\.)?discord\.gg\/([a-zA-Z0-9_]+)/,
        extract: null,
    },
    github: {
        key: 'github',
        name: 'Github',
        url: 'github.com',
        hex: '#24292e',
        wideThumb: true,
        urlMatch: /http(?:s)?:\/\/(?:www\.)?(?:gist\.)?github\.com\/([a-zA-Z0-9_]+)/,
        extract: null,
    },
    facebook: {
        key: 'facebook',
        name: 'Facebook',
        url: 'facebook.com',
        hex: '#4267b2',
        wideThumb: false,
        urlMatch: /http(?:s)?:\/\/(?:www\.)?facebook\.com\/([a-zA-Z0-9_]+)/,
        extract: null,
    },
    redit: {
        key: 'redit',
        name: 'Facebook',
        url: 'redit.com',
        hex: '#4267b2',
        wideThumb: false,
        urlMatch: /http(?:s)?:\/\/(?:www\.)?reddit\.com\/([a-zA-Z0-9_]+)/,
        extract: null,
    }
}

const parse = (link) => {
    // for each link find match by urlMatch
}

export default { platforms, parse } 

// to test stuff you're adding comment out the export above, run this using "node platforms.js" and console log instead
// console.log(platforms['twitch'].extract('https://www.twitch.tv/jamiepinelive'))